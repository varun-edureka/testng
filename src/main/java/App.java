
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;
import org.openqa.selenium.chrome.ChromeOptions;
public class App
{
 @Test

 public static void main(String[] args) {


 System.setProperty("webdriver.chrome.driver","chromedriver.exe");
 ChromeOptions chromeOptions = new ChromeOptions();
 WebDriver driver = new ChromeDriver(chromeOptions);

 System.out.println("Hi, Welcome to Edureka's Live session with Varun on Selenium WebDriver");


 driver.get("http://localhost:8081/addressbook");

 driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
 driver.findElement(By.className("v-button")).click();
 driver.findElement(By.id("gwt-uid-5")).sendKeys("Varun");
 driver.findElement(By.id("gwt-uid-7")).sendKeys("DevOps");
 driver.findElement(By.id("gwt-uid-9")).sendKeys("1234567890");
 driver.findElement(By.id("gwt-uid-11")).sendKeys("varun.edureka@gmail.com");
 driver.findElement(By.id("gwt-uid-13")).sendKeys("01/01/2019");
 driver.findElement(By.className("v-button-primary")).click();
 //Thread.sleep(5000);
 driver.quit();
 }

}
